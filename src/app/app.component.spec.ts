import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ProductListComponent } from './productlist/productlist.component';
import { HeaderComponent } from './header/header.component';
import { CurrentOrderComponent } from './current-order/current-order.component';
import { CreateOrderDialogComponent } from './create-order-dialog/create-order-dialog.component';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { DatePoDialogComponent } from './date-po-dialog/date-po-dialog.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MyOwnCustomMaterialModule } from './material.module';
import { ScrollingModule } from '@angular/cdk/scrolling';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        ProductListComponent,
        CurrentOrderComponent,
        CreateOrderDialogComponent,
        JwPaginationComponent,
        DatePoDialogComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AngularFontAwesomeModule,
        MyOwnCustomMaterialModule,
        ScrollingModule
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
