import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as _ from 'lodash';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  productListPromise: any;
  productList: any = [];
  unfilteredList;
  copyList;

  constructor(private http: HttpClient) {

  }

  //Initial approach to extract data through LOCAL JSON
  getProductList() {
    let that = this;
    return new Promise((resolve, reject) => {
      this.http.get('assets/uniqueProducts.json')
        .subscribe((data) => {
          this.productList = _.cloneDeep(data['products']);
          this.unfilteredList = _.clone(data['products']);
          this.copyList = _.clone(data['products']);
          resolve(that.productList);
        });
    })

  }
  getProductById(id) {
    var product = (_.find(this.productList, ['id', id]));
    return (_.clone(product));
  }
  getJSFdata() {
    
    return this.http.get("http://localhost:7101/order/jersey/MyShoppingListService",{responseType: 'text'});
  }
}
