import { Component, Output, EventEmitter, Inject, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common'; 
import { ProductService } from './services/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


   

export class AppComponent implements OnInit {
  ngOnInit(){
  
  }
  title = 'USFoods';
  inputValue='';
  // @Output() outputToJSP=new EventEmitter();
 
  constructor(private elementRef:ElementRef,@Inject(DOCUMENT) document,private productService:ProductService) {
    this.inputValue = this.elementRef.nativeElement.getAttribute('inputValue');
    console.log("Initial Input on bootstrap",this.inputValue);
  }
  
    //OUTPUT EMITTER TEST
    
    outputEmit() { 
      event.preventDefault();
       this.productService.getJSFdata().subscribe((data)=>{
        console.log("Call API Works"+data);
       })

  }

}